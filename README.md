## Follow the steps specified for your machine an OS:
- [Windows](#windows)
- [Raspberry](#raspberry)
- [Debian](#debian)


## Windows

- Install [sublime](https://www.sublimetext.com/)
- Install [Python](#python)
- Install [pyqtgraph](http://www.pyqtgraph.org/)
- Install [Arduino IDE](#arduino-ide-and-other-boards)
- Install [Eclipse](#eclipse)

## Python
- Download [WinPython 2.7](https://sourceforge.net/projects/winpython/files/WinPython_2.7/)
- Install WinPython at the following address or an address of your choosing
```
C:\Python2.7
```
- Create C:\Python2.7\Register.py and paste the following code

```
#
# script to register Python 2.0 or later for use with 
# Python extensions that require Python registry settings
#
# written by Joakim Loew for Secret Labs AB / PythonWare
#
# source:
# http://www.pythonware.com/products/works/articles/regpy20.htm
#
# modified by Valentine Gogichashvili as described in http://www.mail-archive.com/distutils-sig@python.org/msg10512.html

import sys

from _winreg import *

# tweak as necessary
version = sys.version[:3]
installpath = sys.prefix

regpath = "SOFTWARE\\Python\\Pythoncore\\%s\\" % (version)
installkey = "InstallPath"
pythonkey = "PythonPath"
pythonpath = "%s;%s\\Lib\\;%s\\DLLs\\" % (
    installpath, installpath, installpath
)

def RegisterPy():
    try:
        reg = OpenKey(HKEY_CURRENT_USER, regpath)
    except EnvironmentError as e:
        try:
            reg = CreateKey(HKEY_CURRENT_USER, regpath)
            SetValue(reg, installkey, REG_SZ, installpath)
            SetValue(reg, pythonkey, REG_SZ, pythonpath)
            CloseKey(reg)
        except:
            print "*** Unable to register!"
            return
        print "--- Python", version, "is now registered!"
        return
    if (QueryValue(reg, installkey) == installpath and
        QueryValue(reg, pythonkey) == pythonpath):
        CloseKey(reg)
        print "=== Python", version, "is already registered!"
        return
    CloseKey(reg)
    print "*** Unable to register!"
    print "*** You probably have another Python installation!"

if __name__ == "__main__":
    RegisterPy()
    
```
- Go to 'C:\Python2.7' and open 'WinPython Command Prompt' and type:
```
C:\Python2.7\python2.7.6>cd ..
C:\Python2.7>python Register.py
```

## Arduino IDE and other boards

- Download and instal [Arduino IDE](https://www.arduino.cc/en/Guide/Windows)
- To install ESP 32 Thing [download ZIP](https://github.com/espressif/arduino-esp32) the repository
- Go to 'C:/[Your Arduino folder]/Arduino/hardware' and create folder 'espressif'
- Extract archive in 'C:/[Your Arduino folder]/Arduino/hardware/expressif' and rename it to esp 32
- Open 'C:/[Your Arduino folder]/Arduino/hardware/expressif/esp32/tools' and double-click get.exe
- Plug your ESP32 board and wait for the drivers to install (or install manually any that might be required)
- [MPU6500](https://www.youtube.com/watch?v=V4NdsBjUAO0&t=53s) 
- Pentru citire bluetooth [PyBluez](https://pypi.python.org/pypi/PyBluez)


## Eclipse
- Download [Eclipse IDE for C/C++ Developers](https://www.eclipse.org/downloads/eclipse-packages/)
- Download and Install [JRE](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
- Open Eclipse, go to Help->Install New Software. On the row with "Working with press ADD" and in the URL section paste "http://avr-eclipse.sourceforge.net/updatesite"